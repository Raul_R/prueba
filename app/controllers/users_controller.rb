class UsersController < ApplicationController

  skip_before_action :require_login, only: [:new, :create]

  def new
  @user = User.new
  end

  def create
  @user = User.new(user_params)
    if @user.save
      redirect_to log_in_path, :notice => "Registro completo Porfavor inicie sesion!!"
      else
      render "new"
    end
  end
  def show
    @user = User.find(params[:id])
    @username = User.find(session[:user_id]).nombre
  end

  private

 def user_params
   params.require(:user).permit(:nombre, :email, :password, :password_confirmation, :encrypted_password)
 end
end
