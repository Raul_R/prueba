class ApplicationController < ActionController::Base
  helper_method :current_user
  before_action :require_login
  helper_method :current_user
  protect_from_forgery with: :exception


  private

  def require_login
    unless logged_in?
      flash[:error] = " ----------> ACCESO NO AUTORIZADO <------------- \n\n Porfavor inicia sesion para continuar !!"
      redirect_to log_in_path # halts request cycle
    end
  end

  def logged_in?
    !!current_user
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  
end
