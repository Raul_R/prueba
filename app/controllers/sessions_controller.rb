class SessionsController < ApplicationController

  skip_before_action :require_login, only: [:new, :create]

  def new
  end

  def create
  user = User.authenticate(params[:email], params[:password])
    if user
    session[:user_id] = user.id
    #Modificar esto cuando tenga la vista de sucursales
    redirect_to  root_url, :notice => "Sesion Iniciada!"
    else
    flash.now.alert = "Email o password Invalido"
    render "new"
    end
  end

  def destroy
  session[:user_id] = nil
  redirect_to log_in_path, :notice => "Sesion Cerrada!"
  end
end
