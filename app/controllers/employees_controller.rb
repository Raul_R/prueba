class EmployeesController < ApplicationController

  def index
    @branch = Branch.find(params[:branch_id])
    @employees = @branch.employees
  end

  def show
    @branch = Branch.find(params[:branch_id])
    @employee =Branch.empleados_por_sucursal(branch_id)
  end
 def new
   @employee = Employee.new
 end

 def create
     @branch = Branch.find(params[:branch_id])
     @Employee = @branch.employees.create(employee_params)
     redirect_to branch_path(@branch)
 end

 def edit
     @branch = Branch.find(params[:branch_id])
 end

 def update
   @branch = Branch.find(params[:branch_id])
   if @employee = @branch.employees.update(employee_params)
      redirect_to @branch
   else
     render 'edit'
   end
 end

 def destroy
   @branch = Branch.find(params[:branch_id])
   @employee = @branch.employees.find(params[:id])
   @employee.destroy
   redirect_to branch_path(@branch)
 end

 def import
   @errors = Employee.import(params[:employee][:file],params[:branch_id]) #se agrego [:branch] para buscar :file dentro de la variable
     if @errors.present?
       redirect_to branch_path(params[:branch_id]), notice: 'Error: ' + errors + '   : El archivo no se pudo importar!!'
     else
       redirect_to branch_path(params[:branch_id]), notice: "-------> Empleados importados con Exito!"
     end
 end

 def errors
   @errors.map(&:inspect).join()
 end

   private
   def employee_params
     params.require(:employee).permit(:nombre, :rfc, :puesto)
   end
end
