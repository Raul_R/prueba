# encoding: UTF-8
class BranchesController < ApplicationController

  #before_action :validates,  except: [:index]
  def index
    @branches = Branch.sucursales_por_usuario(current_user.id)   #se enlistan las sucursale spara la descarga
    respond_to do |format|
      format.html
      format.xlsx  {
    response.headers['Content-Disposition'] = 'attachment; filename="Listado_sucursales.xlsx"'
    }
    end
  end

  def new
    @branch = Branch.new
  end

  def create
    @branch = current_user.branches.new(branch_params)
    @branch.user_id = current_user.id  #asignando user_id de branch perteneciente al current user
    if @branch.save
      redirect_to @branch
    else
      render 'new'
    end
  end

  def show
    @branch = Branch.find(params[:id])
    @sucursales = Branch.sucursales_por_usuario(current_user.id) #asignando a la varibale sucursales el valor del metodo self.sucursalespor usuario
  end

  def edit
    @branch = Branch.find(params[:id])
  end

  def update
    @branch = Branch.find(params[:id])
    if @branch.update(branch_params)
      redirect_to branches_path
    else
      render 'edit'
    end
  end

  def destroy
    @branch = Branch.find(params[:id])
    @branch.destroy
    redirect_to branches_path
  end

  def import
    @errors =Branch.import(params[:branch][:file], current_user.id) #se agrego [:branch] para buscar :file dentro de la variable
      if @errors.present?
        redirect_to branches_path, notice: 'Error: ' + errors + '   : El archivo no se pudo importar!!'
      else
        redirect_to branches_path, notice: "-------> Sucursales importadas con Exito!"
      end
  end
  def errors
    @errors.map(&:inspect).join()
  end


  private

    def branch_params
      params.require(:branch).permit(:nombre, :calle, :numExt, :numInt, :cp, :ciudad, :pais, employees_attributes:[:nombre, :rfc, :puesto, :id])
    end
end
