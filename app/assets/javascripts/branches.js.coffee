
BranchesController = Paloma.controller('Branches')

BranchesController::new = ->
  $('#new_branch').submit (ev) ->
    validar = ValidateBranch()
    validarN = ValidateNestedEmploye()
    console.log 'La validacion susucrsal es: ' + validar + ' Val empleado es: ' + validarN
    if validarN == undefined
      validarN = true
    if  validar && validarn != false
      alert 'Sucursal Creada con exito'
    else
      event.preventDefault()


BranchesController::show = ->

  $('#new_employee').submit (ev) ->
    validar = ValidateEmployee()
    console.log 'la funcion tiene el valor ' + validar
    if validar == false
      event.preventDefault()
    else
      alert 'Empleado creado existosamente!!'



ValidateBranch = ->

  flag = true
  nombreb = document.getElementById('branch_nombre').value
  nume = document.getElementById('branch_numExt').value
  numi = document.getElementById('branch_numInt').value
  cp = document.getElementById('branch_cp').value
  p = /^[0-9]+$/

  #Validacion campo nombre sucursal contenga caracteres

  if nombreb.value == null || nombreb.length == 0
    document.getElementById("branch_nombre").style.borderColor = "#E34234"
    alert 'Ingrese el nombre de la sucursal'
    flag = false


  if nume == null || nume.length == 0
    document.getElementById("branch_numExt").style.borderColor = "#E34234"
    alert 'Introduzca el numero exterior'
    flag = false


  if numi == null || numi.length == 0
    document.getElementById("branch_numInt").style.borderColor = "#E34234"
    alert 'Introduzca el numero interior'
    flag = false


  if cp == null || cp.length == 0
    document.getElementById("branch_cp").style.borderColor = "#E34234"
    alert 'Introduzca el Codigo Postal'
    flag = false


  if !/^([0-9])*$/.test(cp)
    document.getElementById("branch_cp").style.borderColor = "#E34234"
    alert 'El codigo postal no es numerico'
    flag = false
  else
    flag = true

    flag


ValidateNestedEmploye = ->
  flag = true

  if document.getElementById('branch_employees_attributes_0_nombre') != null
    nombre = document.getElementById('branch_employees_attributes_0_nombre')
    nombre = 0
    console.log 'valor' + nombre
  else
    flag = false

  if document.getElementById('branch_employees_attributes_0_rfc') != null
    rfc = document.getElementById('branch_employees_attributes_0_rfc')
    rfc = 0
    console.log 'valor' + rfc
  else
    flag = false


  if nombre == 0
    document.getElementById('branch_employees_attributes_0_nombre').style.borderColor = "#E34234"
    alert 'Ingrese el Nombre del empleado'
    flag = false
  else
    flag = false

  if rfc == 0
    document.getElementById("branch_employees_attributes_0_rfc").style.borderColor = "#E34234"
    alert 'Ingrese el RFC del empleado'
    flag  = false
  else
    flag = false

    flag
return
