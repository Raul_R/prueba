
UsersController = Paloma.controller('Users')

UsersController::new = ->
  $('#new_user').submit (ev) ->
    validar = validateRegister()
    console.log 'la funcion tiene el valor ' + validar
    if validar == false
      event.preventDefault()
    else
      alert 'Registro  EXITOSO!!!'


validateRegister = ->

  flag = true
  nombre = document.getElementById('user_nombre')
  email = document.getElementById('user_email')
  password1 = document.getElementById('user_password').value
  password2 = document.getElementById('user_password_confirmation').value

  #Validacion campo nombre contenga caracteres

  if nombre.value == '' or nombre.value == null
  #Compara si el valor "null"
    alert 'El campo nombre esta vacio '
    document.getElementById("user_nombre").style.borderColor = "#E34234"
    flag = false

  #----> Validacion Email
  if email.value == ''
  #Compara si el valor "null"
    alert 'El campo email no puede estar vacio'
    document.getElementById("user_email").style.borderColor = "#E34234"
    flag = false

  #---> Validacion de password inicia aqui
  # Que Los campos contengan caracteres (No campos vacios)
  if password1.length == 0 or password2.length == 0
    document.getElementById("user_password").style.borderColor = "#E34234"
    flag = false

  #Password coincidan
  if password1 != password2
    document.getElementById("user_password").style.borderColor = "#E34234"
    document.getElementById("user_password_confirmation").style.borderColor = "#E34234"
    alert 'Las contrasenas deben de coincidir'
    flag = false

  flag
return
