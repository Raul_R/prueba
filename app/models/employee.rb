# encoding: utf-8
class Employee < ActiveRecord::Base

  belongs_to :branch

  validates_presence_of :nombre, :on => :create
  validates_presence_of :rfc, :on => :create
  before_validation :check, :on => :create

  def empleados_por_sucursal(branch_id)
    Employee.where(branch_id: branch_id)
  end

  def self.import(file,branch_id)
    #se recibe la variable user_id del controlador como parameto a importar
    @errors = []  #<--- array para acumular errores
    spreadsheet = Roo::Spreadsheet.open(file.path, extension: :xlsx)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      employee = find_or_initialize_by(id: row["id"])
      employee.attributes = row.to_hash.slice(*row.to_hash.keys)
      employee.branch_id = branch_id
      if employee.valid?
        employee.save!
      else
        employee.errors.full_messages.each do |message|
          @errors << "La información de la Fila: #{i}, columna: #{message}:"
        end
      end
    end
    @errors
  end
      #se asigna la variable user_id al parametro de user id de la sucursal
  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".xls" then Roo::Excel.new(file.path, packed: nil, file_warning: :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, packed: nil, file_warning: :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  private

  def check
    @flag = true
    if self.nombre.present?
    else
      errors.add(:base, 'Nombre, Esta vacio o es invalido  || CALLBACK ||')
      @flag = false
    end
    if self.rfc.present?
    else
      errors.add(:base, 'RFC, Esta vacio o es invalido  || CALLBACK ||')
      @flag = false
    end
    if self.puesto.present?
    else
      errors.add(:base, 'Puesto, Esta vacio o es invalido  || CALLBACK ||')
      @flag = false
    end
    @flag
  end

end
