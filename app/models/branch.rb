# encoding: utf-8
class Branch < ActiveRecord::Base
#Validacion para que el nombre de la sucursal sea por lo menos de 5 caract
  belongs_to :user
  has_many :employees, dependent: :destroy

  accepts_nested_attributes_for :employees, allow_destroy: true, limit: 5

  validates :nombre, presence: true,
                     length: { minimum: 2 }

  validates_presence_of :numExt, :on => :create
  validates_presence_of :numInt, :on => :create
  validates_numericality_of :cp, :on => :create
  #before_save :default_name, :if => lambda { |branch| branch[:nombre].blank? }


  before_validation :no_empty, :on => :create

  #metodo que busca y regresa las sucursales pertenecientes a un user_id del empleado (ver en sessionControler)
  def self.sucursales_por_usuario(user_id)
    Branch.where(user_id: user_id)
  end


  def self.import(file, user_id)
    #se recibe la variable user_id del controlador como parameto a importar
    @errors = []  #<--- array para acumular errores
    spreadsheet = Roo::Spreadsheet.open(file.path, extension: :xlsx)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      branch = find_or_initialize_by(id: row["id"])
      branch.attributes = row.to_hash.slice(*row.to_hash.keys)
      branch.user_id = user_id
      if branch.valid?
        branch.save!
      else
        branch.errors.full_messages.each do |message|
          @errors << "La información de la Fila: #{i}, columna: #{message}:"
        end
      end
    end
    @errors
  end

      #se asigna la variable user_id al parametro de user id de la sucursal





  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".xls" then Roo::Excel.new(file.path, packed: nil, file_warning: :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, packed: nil, file_warning: :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end


  private

  def no_empty
    @flag = true
    if self.nombre.present?
    else
      errors.add(:base, 'Nombre, Esta vacio o es invalido  || CALLBACK ||')
      @flag = false
    end
    if self.calle.present?
    else
      errors.add(:base, 'Calle, Esta vacio o es invalido  || CALLBACK ||')
      @flag = false
    end
    if self.numExt.present?
    else
      errors.add(:base, 'Numero Exterior, Esta vacio o es invalido  || CALLBACK ||')
      @flag = false
    end
    if self.numInt.present?
    else
      errors.add(:base, 'Numero Interior, Esta vacio o es invalido  || CALLBACK ||')
      @flag = false
    end
    if self.cp.present?
    else
      errors.add(:base, 'Codigo Postal, Esta vacio o es invalido  || CALLBACK ||')
      @flag = false
    end
    if self.ciudad.present?
    else
      errors.add(:base, 'Ciudad, Esta vacio o es invalido  || CALLBACK ||')
      @flag = false
    end
    if self.pais.present?
    else
      errors.add(:base, 'Pais, Esta vacio o es invalido || CALLBACK ||')
      @flag = false
    end
    if self.cp.to_s.match(/(\d)/)
    else
      errors.add(:base, 'Codigo Postal, Es valor no numerico o invalido || CALLBACK ||')
      @flag = false
    end
    @flag
  end


end
