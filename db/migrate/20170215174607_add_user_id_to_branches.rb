class AddUserIdToBranches < ActiveRecord::Migration
  def change
    add_reference :branches, :user, index: true
  end
end
