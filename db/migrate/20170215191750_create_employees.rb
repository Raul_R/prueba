class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :nombre
      t.string :rfc
      t.string :puesto
      t.references :branch

      t.timestamps
    end
  end
end
