class ChangeDataTypeForNumInt < ActiveRecord::Migration

  def self.up
   change_column :branches, :numInt, :string
  end
  def self.down
   change_column :branches, :numInt, :integer
  end

end
