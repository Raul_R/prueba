class ChangeDataTypeForCp < ActiveRecord::Migration

  def self.up
   change_column :branches, :cp, :string
  end
  def self.down
   change_column :branches, :cp, :integer
  end

end
