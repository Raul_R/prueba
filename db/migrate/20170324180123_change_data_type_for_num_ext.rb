class ChangeDataTypeForNumExt < ActiveRecord::Migration

  def self.up
   change_column :branches, :numExt, :string
  end
  def self.down
   change_column :branches, :numExt, :integer
  end

end
