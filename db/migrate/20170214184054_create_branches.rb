class CreateBranches < ActiveRecord::Migration
  def change
    create_table :branches do |t|
      t.string :nombre
      t.string :calle
      t.integer :numExt
      t.integer :numInt
      t.integer :cp
      t.string :ciudad
      t.string :pais

      t.timestamps
    end
  end
end
